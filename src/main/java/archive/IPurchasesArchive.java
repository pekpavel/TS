/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package archive;

import shop.Item;
import shop.Order;

/**
 *
 * @author Karel
 */
public interface IPurchasesArchive {

    int getHowManyTimesHasBeenItemSold(Item item);

    int getNumberOfOrders();

    void printItemPurchaseStatistics();

    void putOrderToPurchasesArchive(Order order);
    
}

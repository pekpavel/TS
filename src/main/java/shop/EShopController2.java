package shop;

import archive.PurchasesArchive;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import storage.*;
import archive.*;


public class EShopController2 {

    private Storage storage;
    private IPurchasesArchive archive;
    private ArrayList<ShoppingCart> carts;
    private ArrayList<Order> orders;

    public EShopController2(
            Storage storage, IPurchasesArchive archive,
            ArrayList<ShoppingCart> carts, ArrayList<Order> orders) {
        this.storage = storage;
        this.archive = archive;
        this.carts = carts;
        this.orders = orders;
    }    

    public void purchaseShoppingCart(ShoppingCart cart, String customerName, String customerAddress) 
            throws NoItemInStorage, ShoppingCartIsEmptyException {
        if (cart.getCartItems().isEmpty()) {
            throw new ShoppingCartIsEmptyException();
        }
        Order order = new Order(cart, customerName, customerAddress);
        storage.processOrder(order);
        archive.putOrderToPurchasesArchive(order);
    }

    public ShoppingCart newCart() {
        ShoppingCart newCart = new ShoppingCart();
        carts.add(newCart);
        return newCart;
    }
}


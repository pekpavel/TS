package mantisbt.pageObjects;

import java.util.Map;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class LoginPageObjectGen {
    private Map<String, String> data;
    private WebDriver driver;
    private int timeout = 15;

    @FindBy(css = "input.button")
    @CacheLookup
    private WebElement login;

    @FindBy(css = "a[href='lost_pwd_page.php']")
    @CacheLookup
    private WebElement lostYourPassword;

    @FindBy(css = "a[href='my_view_page.php']")
    @CacheLookup
    private WebElement mantisbt;

    @FindBy(name = "secure_session")
    @CacheLookup
    private WebElement onlyAllowYourSessionToBe;

    private final String pageLoadedText = "You should disable the default 'administrator' account or change its password";

    private final String pageUrl = "/login_page.php";

    @FindBy(name = "username")
    @CacheLookup
    private WebElement rememberMyLoginInThisBrowser1;

    @FindBy(name = "password")
    @CacheLookup
    private WebElement rememberMyLoginInThisBrowser2;

    @FindBy(name = "perm_login")
    @CacheLookup
    private WebElement rememberMyLoginInThisBrowser3;

    @FindBy(css = "a[href='signup_page.php']")
    @CacheLookup
    private WebElement signupForANewAccount;

    public LoginPageObjectGen() {
    }

    public LoginPageObjectGen(WebDriver driver) {
        this();
        this.driver = driver;
    }

    public LoginPageObjectGen(WebDriver driver, Map<String, String> data) {
        this(driver);
        this.data = data;
    }

    public LoginPageObjectGen(WebDriver driver, Map<String, String> data, int timeout) {
        this(driver, data);
        this.timeout = timeout;
    }

    /**
     * Click on Login Button.
     *
     * @return the LoginPageObjectGen class instance.
     */
    public LoginPageObjectGen clickLoginButton() {
        login.click();
        return this;
    }

    /**
     * Click on Lost Your Password Link.
     *
     * @return the LoginPageObjectGen class instance.
     */
    public LoginPageObjectGen clickLostYourPasswordLink() {
        lostYourPassword.click();
        return this;
    }

    /**
     * Click on Mantisbt Link.
     *
     * @return the LoginPageObjectGen class instance.
     */
    public LoginPageObjectGen clickMantisbtLink() {
        mantisbt.click();
        return this;
    }

    /**
     * Click on Signup For A New Account Link.
     *
     * @return the LoginPageObjectGen class instance.
     */
    public LoginPageObjectGen clickSignupForANewAccountLink() {
        signupForANewAccount.click();
        return this;
    }

    /**
     * Fill every fields in the page.
     *
     * @return the LoginPageObjectGen class instance.
     */
    public LoginPageObjectGen fill() {
        setRememberMyLoginInThisBrowser1PasswordField();
        setRememberMyLoginInThisBrowser2PasswordField();
        setRememberMyLoginInThisBrowser3CheckboxField();
        setOnlyAllowYourSessionToBeCheckboxField();
        return this;
    }

    /**
     * Fill every fields in the page and submit it to target page.
     *
     * @return the LoginPageObjectGen class instance.
     */
    public LoginPageObjectGen fillAndSubmit() {
        fill();
        return submit();
    }

    /**
     * Set Only Allow Your Session To Be Used From This Ip Address Checkbox field.
     *
     * @return the LoginPageObjectGen class instance.
     */
    public LoginPageObjectGen setOnlyAllowYourSessionToBeCheckboxField() {
        if (!onlyAllowYourSessionToBe.isSelected()) {
            onlyAllowYourSessionToBe.click();
        }
        return this;
    }

    /**
     * Set default value to Remember My Login In This Browser Password field.
     *
     * @return the LoginPageObjectGen class instance.
     */
    public LoginPageObjectGen setRememberMyLoginInThisBrowser1PasswordField() {
        return setRememberMyLoginInThisBrowser1PasswordField(data.get("REMEMBER_MY_LOGIN_IN_THIS_BROWSER_1"));
    }

    /**
     * Set value to Remember My Login In This Browser Password field.
     *
     * @param rememberMyLoginInThisBrowser1Value
     * @return the LoginPageObjectGen class instance.
     */
    public LoginPageObjectGen setRememberMyLoginInThisBrowser1PasswordField(String rememberMyLoginInThisBrowser1Value) {
        rememberMyLoginInThisBrowser1.sendKeys(rememberMyLoginInThisBrowser1Value);
        return this;
    }

    /**
     * Set default value to Remember My Login In This Browser Password field.
     *
     * @return the LoginPageObjectGen class instance.
     */
    public LoginPageObjectGen setRememberMyLoginInThisBrowser2PasswordField() {
        return setRememberMyLoginInThisBrowser2PasswordField(data.get("REMEMBER_MY_LOGIN_IN_THIS_BROWSER_2"));
    }

    /**
     * Set value to Remember My Login In This Browser Password field.
     *
     * @return the LoginPageObjectGen class instance.
     */
    public LoginPageObjectGen setRememberMyLoginInThisBrowser2PasswordField(String rememberMyLoginInThisBrowser2Value) {
        rememberMyLoginInThisBrowser2.sendKeys(rememberMyLoginInThisBrowser2Value);
        return this;
    }

    /**
     * Set Remember My Login In This Browser Checkbox field.
     *
     * @return the LoginPageObjectGen class instance.
     */
    public LoginPageObjectGen setRememberMyLoginInThisBrowser3CheckboxField() {
        if (!rememberMyLoginInThisBrowser3.isSelected()) {
            rememberMyLoginInThisBrowser3.click();
        }
        return this;
    }

    /**
     * Submit the form to target page.
     *
     * @return the LoginPageObjectGen class instance.
     */
    public LoginPageObjectGen submit() {
        clickLoginButton();
        return this;
    }

    /**
     * Unset Only Allow Your Session To Be Used From This Ip Address Checkbox field.
     *
     * @return the LoginPageObjectGen class instance.
     */
    public LoginPageObjectGen unsetOnlyAllowYourSessionToBeCheckboxField() {
        if (onlyAllowYourSessionToBe.isSelected()) {
            onlyAllowYourSessionToBe.click();
        }
        return this;
    }

    /**
     * Unset Remember My Login In This Browser Checkbox field.
     *
     * @return the LoginPageObjectGen class instance.
     */
    public LoginPageObjectGen unsetRememberMyLoginInThisBrowser3CheckboxField() {
        if (rememberMyLoginInThisBrowser3.isSelected()) {
            rememberMyLoginInThisBrowser3.click();
        }
        return this;
    }

    /**
     * Verify that the page loaded completely.
     *
     * @return the LoginPageObjectGen class instance.
     */
    public LoginPageObjectGen verifyPageLoaded() {
        (new WebDriverWait(driver, timeout)).until(new ExpectedCondition<Boolean>() {
            public Boolean apply(WebDriver d) {
                return d.getPageSource().contains(pageLoadedText);
            }
        });
        return this;
    }

    /**
     * Verify that current page URL matches the expected URL.
     *
     * @return the LoginPageObjectGen class instance.
     */
    public LoginPageObjectGen verifyPageUrl() {
        (new WebDriverWait(driver, timeout)).until(
                (ExpectedCondition<Boolean>) (WebDriver d) -> d.getCurrentUrl().contains(pageUrl));
        return this;
    }
}


package mantisbt.pageObjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class LoginPageObject {

    private final String baseUrl;

    private final WebDriver driver;

    public LoginPageObject(String baseUrl, WebDriver driver) {
        this.baseUrl = baseUrl;
        this.driver = driver;
    }
    
    public void open() {
        driver.get(baseUrl + "login_page.php");
    }
    
    public void setPassword(String password) {
        driver.findElement(By.name("password")).clear();
        driver.findElement(By.name("password")).sendKeys(password);
    }
    
    public void setUserName(String userName) {
        driver.findElement(By.name("username")).clear();
        driver.findElement(By.name("username")).sendKeys(userName);
    }
    
    public void setPermLogin() {
        driver.findElement(By.name("perm_login")).click();
    }
    
    public void login() {
        driver.findElement(By.cssSelector("input.button")).click();
    }
    
    public void login(String userName, String password) {
        setUserName(userName);
        setPassword(password);
        driver.findElement(By.cssSelector("input.button")).click();
    }
}

package test.mantisbt.recorded;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.*;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.ITestResult;
import org.testng.annotations.*;

import java.io.File;
import java.io.IOException;
import java.util.concurrent.TimeUnit;

import static org.testng.Assert.*;

public class LoginPageTest {

    private WebDriver driver;
    private String baseUrl;
    private boolean acceptNextAlert = true;
    private final StringBuffer verificationErrors = new StringBuffer();

    @BeforeClass(alwaysRun = true)
    public void setUp() throws Exception {
        System.setProperty("webdriver.gecko.driver", "C:\\Utils\\SeleniumDrivers\\geckodriver.exe");
        driver = new FirefoxDriver();
        baseUrl = "http://192.168.99.100:8998/";
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
    }

    @Test
    public void testLoginWithValidCredentials() throws Exception {
        driver.get(baseUrl + "login_page.php");
        driver.findElement(By.name("username")).clear();
        driver.findElement(By.name("username")).sendKeys("administrator");
        driver.findElement(By.name("password")).clear();
        driver.findElement(By.name("password")).sendKeys("root");
        driver.findElement(By.name("perm_login")).click();
        driver.findElement(By.cssSelector("input.button")).click();
        assertTrue(isElementPresent(By.cssSelector("td.login-info-left")));
        driver.findElement(By.linkText("Logout")).click();
    }

    @Test
    public void testLoginWithInvalidCredentials() throws Exception {
        driver.get(baseUrl + "login_page.php");
        driver.findElement(By.name("username")).clear();
        driver.findElement(By.name("username")).sendKeys("administrator");
        driver.findElement(By.name("password")).clear();
        driver.findElement(By.name("password")).sendKeys("notMyPassword");
        driver.findElement(By.cssSelector("input.button")).click();
        String actual = driver.findElement(By.xpath("//font[@color='red'][1]")).getText();
        System.out.println(actual);
        assertEquals(actual, "Your account may be disabled or blocked or the username/password you entered is incorrect.");
    }
    
    @Test
    public void testFailingResultingInScreenshot() throws Exception {
        driver.get(baseUrl + "login_page.php");
        driver.findElement(By.name("username")).clear();
        driver.findElement(By.name("username")).sendKeys("administrator");
        driver.findElement(By.name("password")).clear();
        driver.findElement(By.name("password")).sendKeys("notMyPassword");
        driver.findElement(By.cssSelector("input.button")).click();
        String actual = driver.findElement(By.xpath("//font[@color='red'][1]")).getText();
        System.out.println(actual);
        assertEquals(actual, "You shall not pass!");
    }

    @AfterClass(alwaysRun = true)
    public void tearDown() throws Exception {
        driver.quit();
        String verificationErrorString = verificationErrors.toString();
        if (!"".equals(verificationErrorString)) {
            fail(verificationErrorString);
        }
    }

    private boolean isElementPresent(By by) {
        try {
            driver.findElement(by);
            return true;
        } catch (NoSuchElementException e) {
            return false;
        }
    }

    private boolean isAlertPresent() {
        try {
            driver.switchTo().alert();
            return true;
        } catch (NoAlertPresentException e) {
            return false;
        }
    }

    private String closeAlertAndGetItsText() {
        try {
            Alert alert = driver.switchTo().alert();
            String alertText = alert.getText();
            if (acceptNextAlert) {
                alert.accept();
            } else {
                alert.dismiss();
            }
            return alertText;
        } finally {
            acceptNextAlert = true;
        }
    }

    @AfterMethod
    public void takeScreenShotOnFailure(ITestResult testResult) throws IOException {
        if (testResult.getStatus() == ITestResult.FAILURE) {
            File scrFile = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
            String fileName = 
                            "G:\\work\\cvut\\6\\TestNgSeleniumFirexox\\output"
                            + testResult.getName()+ ".jpg";
            System.out.println("Copying screenshot from " + scrFile.getAbsolutePath() + " to " + fileName);
            FileUtils.copyFile(scrFile, new File(fileName));
        }
    }
}

package storage;

import org.junit.Assert;
import org.junit.Test;
import shop.Item;

import java.util.Collection;

/**
 * Created by Pavel Pek on 17.04.2017.
 */
public class StorageTest {
//Domaci ukol

    @Test
    public void insertItems() throws Exception {
        Storage newStorage = new Storage();
        newStorage.insertItems(new Item(1, "Banana", 53, "Fruit"), 34); //Vlozeni bananu do storage
        newStorage.insertItems(new Item(2, "Apple", 24, "Fruit"), 25); //Vlozeni jablek do storage

        int countReturned = newStorage.getItemCount(1); //Vracim si kolik bananu je na sklade
        Collection collection = newStorage.getStockEntries(); //Kopie kolekce, kterou si udrzuje sklad

        Assert.assertEquals("Pocet produktu na sklade nesedi", 2, collection.size());
        Assert.assertEquals(34, countReturned); //Ocekavame 34 bananu na sklade

    }

    @Test
    public void removeItems() throws Exception {
        Storage newStorage = new Storage();
        newStorage.insertItems(new Item(1, "Banana", 53, "Fruit"), 34); //Vlozeni bananu do storage

        newStorage.removeItems(new Item(1, "Banana", 53, "Fruit"), 15); //Odeberu 15 bananu

        Assert.assertEquals("Pocet bananu na sklade nesedi", 19, newStorage.getItemCount(1));


    }

}
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.FixMethodOrder;
import org.junit.runners.MethodSorters;

/**
 *
 * @author Karel
 */
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class ScitaniTest {
    
    public ScitaniTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
        System.out.println("setUpClass");
    }
    
    @AfterClass
    public static void tearDownClass() {
        System.out.println("tearDownClass");
    }
    
    @Before
    public void setUp() {
        System.out.println("setUp");
    }
    
    @After
    public void tearDown() {
        System.out.println("tearDown");
    }

    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    @Test
    public void hello() {
        System.out.println("hello");
        // setup/arrange
        int a = 3; int b = 4;
        // act
        int c = a + b;
        // assert
        assertEquals("Spatne se to secetlo",
            7, // ocekavana hodnota
            c); // aktualni hodnota
    }
    
    @Test
    public void hello2() {
        System.out.println("hello2");
    }
}

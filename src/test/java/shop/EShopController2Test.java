package shop;

import java.util.ArrayList;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import storage.NoItemInStorage;
import storage.Storage;
import static org.mockito.Mockito.*;
import archive.IPurchasesArchive;

public class EShopController2Test {
    
    public EShopController2Test() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    @Test
    public void testWithMock() 
            throws NoItemInStorage, ShoppingCartIsEmptyException {
        // mock http://www.vogella.com/tutorials/Mockito/article.html
        IPurchasesArchive archiveMock = mock(IPurchasesArchive.class);
        // vlastni implementace chovani: when(archive.getNumberOfOrders()).thenReturn(1234);
        
        // setup
        ItemImpl item = new ItemImpl(1, "itemName", 10, "category1");
        Storage storage = new Storage();
        storage.insertItems(item, 10);
        
        ArrayList<ShoppingCart> carts = new ArrayList<>();
        ArrayList<Order> orders = new ArrayList<>();
        
        EShopController2 eShopController = new EShopController2(
            storage, archiveMock, carts, orders);
        
        ShoppingCart cart = new ShoppingCart();
        String customerName = "customer";
        String customerAddress = "address";
        cart.addItem(item);
        
        // act
        eShopController.purchaseShoppingCart(cart, customerName, customerAddress);
        // assert
        // verifikace poctu volani metod mock objektu http://www.baeldung.com/mockito-verify
        // lze i verifikovat, ze se metoda volala s urcitymi argumenty
        
        // overi se, ze se metoda putOrderToPurchasesArchive volala 1x s parametrem typu Order 
        // (na hodnote parametru zde nezalezi)
        verify(archiveMock, times(1)).putOrderToPurchasesArchive(any(Order.class));        
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package shop;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Karel
 */
public class ItemImplTest {
    
    public ItemImplTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    @Test
    public void constructorTest() {
        // setup
        ItemImpl item = new ItemImpl(1, "", 2.4f, "");
        // act
        // assert
    }    
    
    @Test
    public void toStringTest() {
        // setup
        ItemImpl item = new ItemImpl(1, "X", 2.4f, "Y");
        // act
        String result = item.toString();
        // assert
        assertEquals("Item   ID 1   NAME X   CATEGORY Y", result);
    }
    
    @Test
    public void getIDTest() {
        // setup
        ItemImpl item = new ItemImpl(1, "X", 2.4f, "Y");
        // act
        int result = item.getID();
        // assert
        assertEquals(1, result);
    }
    
    @Test
    public void setIDTest() {
        // setup
        ItemImpl item = new ItemImpl(1, "X", 2.4f, "Y");
        // act
        item.setID(123);
        int result = item.getID();
        // assert
        assertEquals(123, result);
    }
    
    // item_is_not_equal_to_string
    @Test
    public void equalsTest_ItemAndString_false() {
        // setup
        ItemImpl itemA = new ItemImpl(1, "X", 2.4f, "Y");
        String stringB = "";
        // act
        boolean result = itemA.equals(stringB);
        // assert
        assertFalse(result);    
    }
    
    @Test
    public void equalsTest_ItemAndItem_true() {
        // setup
        ItemImpl itemA = new ItemImpl(1, "X", 2.4f, "Y");
        ItemImpl itemB = new ItemImpl(1, "X", 2.4f, "Y");
        // act
        boolean result = itemA.equals(itemB);
        // assert
        assertTrue(result);    
    }
    
    @Test
    public void equalsTest_ItemAndItemWithNull_true() {
        // setup
        ItemImpl itemA = new ItemImpl(1, null, 2.4f, "Y");
        ItemImpl itemB = new ItemImpl(1, "X", 2.4f, "Y");
        // act
        boolean result = itemA.equals(itemB);
        // assert
        assertFalse(result);    
    }
}

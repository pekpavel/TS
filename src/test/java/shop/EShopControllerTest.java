/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package shop;

import archive.PurchasesArchive;
import java.util.ArrayList;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import storage.NoItemInStorage;
import storage.Storage;

/**
 *
 * @author Karel
 */
public class EShopControllerTest {
    
    public EShopControllerTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    @Test(expected = ShoppingCartIsEmptyException.class)
    public void exceptionIsThrownWhenPurchasingEmptyCart() 
            throws NoItemInStorage, ShoppingCartIsEmptyException {
        // setup
        Storage storage = new Storage();
        archive.PurchasesArchive archive = new PurchasesArchive();
        ArrayList<ShoppingCart> carts = new ArrayList<>();
        ArrayList<Order> orders = new ArrayList<>();
        
        EShopController eShopController = new EShopController(
            storage, archive, carts, orders);
        
        ShoppingCart cart = new ShoppingCart();
        String customerName = "customer";
        String customerAddress = "address";
        // act
        eShopController.purchaseShoppingCart(cart, customerName, customerAddress);
        // assert
        int ordersInArchive = archive.getNumberOfOrders();
        assertEquals(0, ordersInArchive);
    }
}
